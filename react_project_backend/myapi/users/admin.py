from django.contrib import admin
from myapi.users.models import Contact

class ContactAdmin(admin.ModelAdmin):
    pass

admin.site.register(Contact, ContactAdmin)