# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate
from rest_framework import generics
from myapi.models import MyUser
from myapi.users.models import Contact
from myapi.users.serializers import ContactSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework.authtoken.models import Token


class ContactApi(generics.ListCreateAPIView, generics.UpdateAPIView):
    """
    List all snippets, or create a new snippet.
    """
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def create_json(self, contacts, dict=True):
        if dict:
            return {
                'id': contacts.id,
                'user_id': contacts.user_id,
                'first_name': contacts.first_name,
                'last_name': contacts.last_name,
                'title': contacts.title,
                'number': contacts.number,
                'country': contacts.country,
                'avatar_url': contacts.avatar_url,
                'created_at': contacts.created_at,
            }
        contact_list = []
        for contact in contacts:
            contact_list.append({
                'id': contact.id,
                'user_id': contact.user_id,
                'first_name': contact.first_name,
                'last_name': contact.last_name,
                'title': contact.title,
                'number': contact.number,
                'country': contact.country,
                'avatar_url': contact.avatar_url,
                'created_at': contact.created_at,
            })
        return contact_list

    def list(self, request, id=None, user_id=None, contact_id=None):
        # Note the use of `get_queryset()` instead of `self.queryset`
        try:
            queryset = self.get_queryset()
            if id:
                try:
                    a = queryset.get(id=id)
                    data = self.create_json(a, True)
                except Contact.DoesNotExist:
                    return {
                        'status': 404,
                        'message': "Contact not found",
                        'error': True,
                    }
            if user_id:
                try:
                    a = queryset.filter(user_id=user_id)
                    data = self.create_json(a, False)
                except Contact.DoesNotExist:
                    return {
                        'status': 404,
                        'message': "Contacts not found",
                        'error': True,
                }
            if contact_id:
                try:
                    a = queryset.get(id=contact_id)
                    a.delete()
                except Contact.DoesNotExist:
                    return {
                        'status': 404,
                        'message': "Contacts not found",
                        'error': True,
                    }
                return Response({"message": "Record deleted successfully", "status": 200})
            # else:
            #     data = [self.create_json(a) for a in queryset]
        except Exception as ex:
            data = {
                'status': 500,
                'error': True,
                'message': str(ex.args),
            }
        return Response(data)

    def create(self, request, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        try:
            if isinstance(request.data, list):
                lst = [Contact(**r) for r in request.data]
                Contact.objects.bulk_create(lst)
            else:
                Contact.objects.create(**request.data)
        except Exception as ex:
            return Response({
                'status': 500,
                'error': True,
                'message': str(ex.args),
            })
        return Response(request.data)

    def update(self, request, id, *args, **kwargs):
        queryset = self.get_queryset()
        try:
            a = queryset.get(id=id)
            queryset.filter(id=id).update(**request.data)
            return Response({
                'status': 200,
                'message': "Contact updated",
                'error': False,
            })
        except Contact.DoesNotExist:
            return Response({
                'status': 404,
                'message': "Contact not found",
                'error': True,
            })
        except Exception as ex:
            return Response({
                'status': 500,
                'error': True,
                'message': str(ex.args),
            })


class UserApi(generics.ListCreateAPIView):
    """
    List all snippets, or create a new snippet.
    """
    queryset = MyUser.objects.all()
    serializer_class = ContactSerializer
    # permission_classes = (IsAdminUser,)

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        try:
            queryset = self.get_queryset()

            return Response([{
                'id': a.id,
                'first_name': a.first_name,
                'password': a.password,
                'last_name': a.last_name,
                'date_of_birth': a.date_of_birth,
                'email': a.email
            } for a in queryset])
        except Exception as ex:
            return Response({
                'status': 500,
                'error': True,
                'message': str(ex.args),
            })

    def create(self, request, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        try:
            user = MyUser.objects.create(**request.data)

            user.set_password(request.data.get('password', '123'))
            user.save()

            token, created = Token.objects.get_or_create(user=user)
            user_data = {
                "id": user.id,
                "token": token.key,
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name
            }

            return Response(user_data)
        except Exception as ex:
            return Response({
                'status': 500,
                'error': True,
                'message': str(ex.args),
            })


class CustomAuthToken(ObtainAuthToken, generics.CreateAPIView):
    def create(self, request, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        id = ""
        email = ""
        token = None
        status = 403
        message = "Login failed"
        try:
            user = authenticate(username=request.data['email'], password=request.data['password'])
            if user and user.is_authenticated():
                token, created = Token.objects.get_or_create(user=user)
                token = token.key
                id = user.id
                email = user.email
                status = 200
                message = "Login success"

            return Response({
                'token': token,
                'id': id,
                'email': email,
                'status': status,
                'message': message,
            })
        except Exception as ex:
            return Response({
                'status': 500,
                'error': True,
                'message': str(ex.args),
            })

