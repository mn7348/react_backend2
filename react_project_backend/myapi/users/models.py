# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Contact(models.Model):
    user = models.ForeignKey('myapi.MyUser', related_name='contacts')
    title = models.CharField(max_length=10)
    number = models.CharField(max_length=50)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    avatar_url = models.URLField()
    created_at = models.DateTimeField()

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)
