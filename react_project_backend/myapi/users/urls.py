
from django.conf.urls import url
from myapi.users.views import *

urlpatterns = [
    # url(r'^contacts/(?P<user_id>\d+)$',  ContactApi.as_view(), name='contacts_list'),
    url(r'^contacts/(?P<id>\d+)/$',  ContactApi.as_view(), name='contact_detail'),
    url(r'^contacts/user/(?P<user_id>\d+)/$',  ContactApi.as_view(), name='contact_detail'),
    url(r'^contacts/delete/(?P<contact_id>\d+)/$',  ContactApi.as_view(), name='contact_detail'),
    url(r'^login$', CustomAuthToken.as_view(), name='user_login_api'),
    url(r'^signup$',  UserApi.as_view(), name='user_api'),
    url(r'^user$', UserApi.as_view(), name='user_api'),
]
