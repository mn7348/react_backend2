from rest_framework import serializers

from myapi.users.models import Contact


class ContactSerializer(serializers.Serializer):
    class Meta:
        model = Contact
        fields = ('id', 'first_name', )